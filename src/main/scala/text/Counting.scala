package text

import scala.collection.immutable.ListMap

class Counting {

  val cleaner = new TextCleaner

  def totalLetters(text: String): Int = {
    text.length
  }

  def wordsStartWith(words: Array[String], target: String): Int = {
    words.count(w => w.startsWith(target))
  }

  def wordsEndWith(words: Array[String], target: String): Int = {
    words.count(w => w.endsWith(target))
  }

  def countOccurances(text: String, find: String): Int = {
    find.r.findAllIn(text).length
  }

  def countLetters(text: String, letters: String): Int = {
    val onlyTheseLetters = cleaner.purgeLetters(text, letters)
    onlyTheseLetters.length
  }

  def countBigrams(text: String, abc: String): Map[String, Int] = {
    generateBigrams(abc).map(bigram => bigram -> countOccurances(text, bigram)).toMap
  }

  def countTrigrams(text: String, abc: String): Map[String, Int] = {
    generateTrigrams(text, abc).map(trigram => trigram -> countOccurances(text, trigram)).toMap
  }

  def countSkipgrams(text: String, abc: String): Map[String, Int] = {
    generateSkipGrams(abc).map(skipgram => skipgram -> countOccurances(text, skipgram)).toMap
  }

  def generateBigrams(abc: String): Array[String] = {
    abc.toCharArray.flatMap(letter => abc.toCharArray.map(letter2 => s"${letter}${letter2}"))
  }

  def generateTrigrams(text: String,abc: String): Array[String] = {
    val counts =  countBigrams(text, abc)
    val top25bigramscount = ListMap(counts.toSeq.sortWith(_._2 > _._2):_*).splitAt(25)._1
    val top25bigrams = top25bigramscount.keys
    //We use the fact the top 25 trigrams will ALWAYS contain one of the top 25 bigrams
    val trigrams = abc.toCharArray.flatMap(letter => top25bigrams.map(bigram => s"${letter}${bigram}")) ++ abc.toCharArray.flatMap(letter => top25bigrams.map(bigram => s"${bigram}${letter}"))
    trigrams.distinct
  }

  def generateSkipGrams(abc: String): Array[String] = {
    generateBigrams(abc).map(bigram => bigram.toCharArray.mkString("."))
  }
}
