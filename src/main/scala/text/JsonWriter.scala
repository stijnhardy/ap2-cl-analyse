package text

import org.apache.log4j.Logger

import java.io.{File, PrintWriter}

class JsonWriter {
  val basePath = "D:\\analyses\\";
  val processor = new Processor
  val LOG = Logger.getLogger(this.getClass)
  
  def writeMap(input: Map[String,Int]): String = {
    val quoted = input.map(entry => s"\"${entry._1}\":\"${entry._2}\"")
    "{" + quoted.mkString(",") + "}"
  }

  def writeAna(lang: String, ana: Int): Unit = {
    LOG.info("writing analyses")
    val writer = new PrintWriter(new File(basePath + lang + s"${ana}.json"))
    ana match {
      case 1 => writer.write(writeMap(processor.countStartLettersForLanguage(lang)))
      case 2 => writer.write(writeMap(processor.countEndLettersForLanguage(lang)))
      case 3 => writer.write(writeMap(processor.countLettersOverallForLanguage(lang)))
      case 4 => writer.write(writeMap(processor.countVowelsAndConsenants(lang)))
      case 5 => writer.write(writeMap(processor.top25BigramsStarts(lang)))
      case 6 => writer.write(writeMap(processor.top25BigramsEnds(lang)))
      case 7 => writer.write(writeMap(processor.top25Bigrams(lang)))
      case 8 => writer.write(writeMap(processor.top25Trigrams(lang)))
      case 9 => writer.write(writeMap(processor.top25Skipgrams(lang)))
      case 10 => writer.write(writeMap(processor.top25SkipgramsFrequencyOfBigrams(lang)))
    }
    writer.close()
  }
}
