package text

import scala.collection.immutable.ListMap

class Processor {
  val languages = Map(
      "Deens" -> Array("abcdefghijklmnopqrstuvwxyzæåø","aeiouæåø","bcdfghjklmnpqrstvwxyz"),
      "Fins" -> Array("abcdefghijklmnopqrstuvwxyzäö","aeiouäö","bcdfghjklmnpqrstvwxyz"),
      "Italiaans" -> Array("abcdefghilmnopqrstuvz","aeiou","bcdfghlmnpqrstvz"),
      "Nederlands" -> Array("abcdefghijklmnopqrstuvwxyz","aeiou","bcdfghjklmnpqrstvwxyz"),
      "Engels" -> Array("abcdefghijklmnopqrstuvwxyz","aeiou","bcdfghjklmnpqrstvwxyz"),
      "Duits" -> Array("abcdefghijklmnopqrstuvwxyzäöüß","aeiouäöü","bcdfghjklmnpqrstvwxyzß"),
      "Portugees" -> Array("abcdefghijklmnopqrstuvwxyz","aeiou","bcdfghjklmnpqrstvwxyz"),
      "Spaans" -> Array("abcdefghijklmnñopqrstuvwxyz","aeiou","bcdfghjklmnñpqrstvwxyz"),
    )

  val counting = new Counting
  val cleaning = new TextCleaner

  def textForLanguage(language: String): String = {
    scala.io.Source.fromFile("src/main/scala/data/" + language + ".txt").mkString.toLowerCase;
  }

  def abcForLanguage(lang: String): String = {
    languages.get(lang).get(0)
  }

  def totalLetters(language: String): Map[String, Int] = {
    Map("amount" -> textForLanguage(language).length)
  }

    //1. Voor elke letter van het alfabet van de taal in kwestie het aantal woorden dat er mee begint.

  def countStartLettersForLanguage(lang: String): Map[String, Int] = {
    val abc = abcForLanguage(lang)
    val text = textForLanguage(lang)
    val result = abc.toCharArray.map(char => s"${char}" -> counting.wordsStartWith(cleaning.textToWords(text,abc), s"${char}")).toMap
    sortResult(result)
  }

  //2. Voor elke letter van het alfabet van de taal in kwestie het aantal woorden dat er mee eindigt.

  def countEndLettersForLanguage(lang: String): Map[String, Int] = {
    val abc = abcForLanguage(lang)
    val text = textForLanguage(lang)
    val result = abc.toCharArray.map(char => s"${char}" -> counting.wordsEndWith(cleaning.textToWords(text,abc), s"${char}")).toMap
    sortResult(result)
  }

  //3. Voor elke letter van het alfabet van de taal in kwestie de frequentie over de hele taal.

  def countLettersOverallForLanguage(lang: String): Map[String, Int] = {
    val abc = abcForLanguage(lang)
    val text = textForLanguage(lang)
    val result = abc.toCharArray.map(char => s"${char}" -> counting.countOccurances(cleaning.cleantext(text,abc), s"${char}")).toMap
    sortResult(result)
  }

  //4. Voor de vowels en de consonants de verdeling over de hele taal, als groep dan. Er komen bvb
  //30 % vowels en 70 % consonants voor in een taal.

  def countVowelsAndConsenants(lang: String): Map[String, Int] = {
    val vowels = languages.get(lang).get(1)
    val cons = languages.get(lang).get(2)
    val text = textForLanguage(lang)
    val result = Map("vowels" -> counting.countLetters(text, vowels), "consenants" -> counting.countLetters(text, cons))
    sortResult(result)
  }

  //5. Voor de 25 meest frequente bigrams het aantal woorden dat er mee begint.
  def top25BigramsStarts(lang: String): Map[String,Int] = {
    val abc = abcForLanguage(lang)
    val words = cleaning.textToWords(textForLanguage(lang), abc)
    val bigrams = counting.generateBigrams(abc)
    val counts = bigrams.map(bigram => bigram -> counting.wordsStartWith(words, bigram)).toMap
    sortResult(getTop25(counts))
  }

  //6. Voor de 25 meest frequente bigrams het aantal woorden dat er mee eindigt.

  def top25BigramsEnds(lang: String): Map[String,Int] = {
    val abc = abcForLanguage(lang)
    val words = cleaning.textToWords(textForLanguage(lang), abc)
    val bigrams = counting.generateBigrams(abc)
    val counts = bigrams.map(bigram => bigram -> counting.wordsEndWith(words, bigram)).toMap
    sortResult(getTop25(counts))
  }

  //7. Voor de 25 meest frequente bigrams de frequentie van voorkomen.

  def top25Bigrams(lang: String): Map[String,Int] = {
    val abc = abcForLanguage(lang)
    val text = textForLanguage(lang)
    val counts = counting.countBigrams(text, abc)
    sortResult(getTop25(counts))
  }

  //8. Voor de 25 meest frequente trigrams de frequentie van voorkomen.
  def top25Trigrams(lang: String): Map[String,Int] = {
    val abc = abcForLanguage(lang)
    val text = textForLanguage(lang)
    val counts = counting.countTrigrams(text, abc)
    sortResult(getTop25(counts))
  }

  //9. De frequenties van de 25 meest voorkomende skipgrams : enkel trigrams met skip = 2, zoals
  //in het voorbeeld hierboven.

  def top25Skipgrams(lang: String): Map[String,Int] = {
    val abc = abcForLanguage(lang)
    val text = textForLanguage(lang)
    val counts = counting.countSkipgrams(text, abc)
    val result = ListMap(counts.toSeq.sortWith(_._2 > _._2):_*).splitAt(25)._1
    sortResult(result)
  }

  //10.  Voor de 25 meest voorkomende skipgrams: de frequentie van het overeenkomende bigram.
  def top25SkipgramsFrequencyOfBigrams(lang: String): Map[String,Int] = {
    val abc = abcForLanguage(lang)
    val text = textForLanguage(lang)
    val counts = counting.countSkipgrams(text, abc)
    val topSkipgrams = ListMap(counts.toSeq.sortWith(_._2 > _._2):_*).splitAt(25)._1
    val bigramsOfTopSkipgrams = topSkipgrams.map(skipgram => s"${skipgram._1.charAt(0)}${skipgram._1.charAt(2)}")
    val result = bigramsOfTopSkipgrams.map(bigram => bigram -> counting.countOccurances(text, bigram)).toMap
    sortResult(result)
  }

  def getTop25(counts: Map[String,Int]): Map[String,Int] = {
    ListMap(counts.toSeq.sortWith(_._2 > _._2):_*).splitAt(25)._1
  }

  def sortResult(result: Map[String, Int]): Map[String, Int] = {
    ListMap(result.toSeq.sortBy(_._1):_*)
  }

}
