package text

import org.apache.log4j.Logger

class Preprocessor {
  val LOG = Logger.getLogger(this.getClass)

  val processor = new Processor
  val cleaner = new TextCleaner
  val counter = new Counting

  def preprocess(language: String): Unit = {
    logWords(language)
    logPunctuation(language)
    logCapitals(language)
    logNonCapitals(language)
  }

  //voor regex info zie https://regex101.com/
  //Logging the number of words
  def logWords(language: String): Unit = {
    val text = processor.textForLanguage(language)
    val abc = processor.abcForLanguage(language)
    LOG.info(s"Het aantal woorden voor ${language} is : ${cleaner.textToWords(text, abc).length}")
  }

  //Logging the number of punctuation marks, zie https://www.examenoverzicht.nl/nederlands/leestekens
  def logPunctuation(language: String): Unit = {
    val text = processor.textForLanguage(language)
    LOG.info(s"Het aantal leestekens voor ${language} is : ${counter.countOccurances(text, "[.!?,:;\")(\\-]")}")
  }

  //Logging the number of capitals
  def logCapitals(language: String): Unit = {
    val text = processor.textForLanguage(language)
    LOG.info(s"Het aantal hoofdletters voor ${language} is : ${counter.countOccurances(text, "[A-Z]")}")
  }

  //Logging the number of capitals
  def logNonCapitals(language: String): Unit = {
    val text = processor.textForLanguage(language)
    LOG.info(s"Het aantal kleine letters voor ${language} is : ${counter.countOccurances(text, "[a-z]")}")
  }

}
