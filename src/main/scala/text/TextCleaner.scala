package text

class TextCleaner {
  def textToWords(text: String, abc: String): Array[String] = {
      cleantext(text, abc).split(' ')
  }

  //When doing ngram analysis were only interested in sequences of members of our abc
  def abcToNegativeRegex(abc: String): String = {
    "[^" + abc.mkString("|") + "]"
  }

  //In some cases they should be replaced with a space
  def cleantext(text: String, abc: String): String = {
    text.replaceAll(abcToNegativeRegex(abc), " ")
  }

  //In some cases they should be removed
  def purgeLetters(text: String, letters: String): String = {
    text.replaceAll(abcToNegativeRegex(letters), "")
  }
}
