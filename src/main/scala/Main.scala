import org.apache.log4j.{Level, Logger}
import text.{JsonWriter, Preprocessor, Processor}

object Main {
  Logger.getRootLogger.setLevel(Level.DEBUG)

  def main(args: Array[String]): Unit =
  {
    val processor = new Processor
    val preprocessor = new Preprocessor
    val json = new JsonWriter
    val languages = processor.languages

    languages.keys.foreach(lang => preprocessor.preprocess(lang))
    for (i <- 1 to 10) {
      languages.keys.foreach(lang => json.writeAna(lang, i))
    }
  }
}
